# DIRSSI #

Dirssi is a full customizable irssi theme, offering a better comunication highlighting the conversation between people. It comes with a sixteen color palette and a set of monotones colors. To enhance readability I used a set of ten symbols, seven of which are used to show the user actions and the other three, used to wrap the meta content.

### The color palette (True Color) ###
If your terminal support true color, you can enjoy a depth of 24 bit (16,777,216 colors)

![palette.png](https://bitbucket.org/repo/ozjBj9/images/3845249866-palette.png)

The list of hex values come right here:

* Dark Black = #2B3133
* Dark Red = #A22940
* Dark Green = #2B8E3C
* Dark Yellow = #B88412
* Dark Blue = #36599E
* Dark Magenta = #844EA3
* Dark Cyan = #249494
* Dark White = #79878F

* Light Black = #4A4A4A
* Light Red = #FA4356
* Light Green = #98CF23
* Light Yellow = #E8BE27
* Light Blue = #42A6ED
* Light Magenta = #D049f5
* Light Cyan = #5ED4D6
* Light White = #C0C3CC

### The color palette (256 colors) ###
If your terminal support only 256 colors (8-bit depth)

![palette256.png](https://bitbucket.org/repo/ozjBj9/images/1868991217-palette256.png)

The list of HEX values come right here:

* Dark Black = "303030";
* Dark Red = "AF0000";
* Dark Green = "008700";
* Dark Yellow = "D78700";
* Dark Blue = "005FAF";
* Dark Magenta = "875FAF";
* Dark Cyan = "008787";
* Dark White = "8A8A8A";

* Light Black = "4E4E4E";   
* Light Red = "FF0000";
* Light Green = "87D700";
* Light Yellow = "FFDF00";
* Light Blue = "00AFFF";
* Light Magenta = "D75FFF";
* Light Cyan = "5FD7D7";
* Light White = "DADADA"; 

### Screenshots (true colours 24bit) ###
![dirssi_shot_01.png](https://bitbucket.org/repo/ozjBj9/images/2038155589-dirssi_shot_01.png)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact